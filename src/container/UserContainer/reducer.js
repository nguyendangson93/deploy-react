/** @format */

const initialValue = {
  role: 0,
}

export const userFilterReducer = (state = initialValue, action) => {
  switch (action.type) {
    case 'UPDATE_ROLE_FILTER':
      return {
        ...state,
        role: action.roleId,
      }

    default:
      return state
  }
}

