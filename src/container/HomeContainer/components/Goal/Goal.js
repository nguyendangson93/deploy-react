import style from "./Goal.module.css";
import Image from "next/future/image";
import goal from "../../../../../public/goal.png"

export default function Goal() {
  return (
    <div className={style.wrapper}>
      <h2 className={style.header}>MỤC TIÊU</h2>
      <Image
        width={1920}
        height={733}
        layout="responsive"
        className={style.img}
        src={goal}
      />
    </div>
  );
}
