import Form from '../form/Form'

const index = () => {
  return (
    <section >
      <Form />
    </section>
  )
}

export default index
