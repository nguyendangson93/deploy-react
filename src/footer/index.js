import style from "../../styles/Add.module.scss";
import Content40 from "../UI/Content40";
import Wrapper from "../UI/Wrapper";
import Title from "../UI/Title";
import Container from "../UI/Container";
import Line from "../UI/Line";
import Image from "next/future/image";
import { NavLink } from "react-bootstrap";
import bniFooter from "../../public/bni-footer.png";
import linkfb from "../../public/linkFB.png";

const index = () => {
  return (
    <Wrapper otherClass={style.footer}>
      <Container
        ortherStyle={{ display: "flex", flexWrap: "wrap", margin: "auto" }}
      >
        <div className={style.img20}>
          <Image
            layout="responsive"
            width={4389}
            height={3002}
            src={bniFooter}
          />
        </div>
        <Content40>
          <Title
            style={{
              color: "white",
              fontSize: "1.25rem",
              fontWeight: 600,
              marginBottom: 12,
              textAlign: "left",
              padding: 0,
            }}
          >
            BNI - BUSINESS NETWORK INTERNATIONAL
          </Title>
          <p style={{ color: "white" }}>
            BNI - Business Network International là tổ chức kết nối thương mại
            lớn nhất và thành công nhất trên thế giới hiện nay, được Tiến sĩ
            Ivan Misner sáng lập vào năm 1985.
          </p>
        </Content40>
        <Content40>
          <Title
            style={{
              color: "white",
              fontSize: "1.25rem",
              fontWeight: 600,
              textAlign: "left",
              padding: 0,
            }}
          >
            CONTACT INFO
          </Title>
          <div className={style.contact}>
            <NavLink className={style.footerLink} href="https://bnicbd.com/">
              Website: bnicbd.com
            </NavLink>
            <NavLink className={style.footerLink} href="tel:1800.8087">
              Phone : 1800.8087
            </NavLink>
            <NavLink className={style.footerLink} href="mailto:hcmc6@bni.vn">
              Email: hcmc6@bni.vn
            </NavLink>
            <NavLink
              className={style.footerLink}
              href="www.facebook.com/BNI.HCM.Central6/"
            >
              Fanpage: www.facebook.com/BNI.HCM.Central6/
            </NavLink>
            <NavLink href="https://www.facebook.com/BNI.HCM.Central6">
              <Image
                width={340}
                height={131}
                layout="responsive"
                className={style.iframeFooter}
                src={linkfb}
              />
            </NavLink>
          </div>
        </Content40>
        <Line style={{ marginBottom: 20 }} />
        <Title
          style={{
            color: "white",
            fontSize: 16,
            fontWeight: 400,
            margin: "auto",
            paddingBottom: 30,
          }}
        >
          © 2022 All Rights Reserved By VIET JAPAN DIGITAL CO., LTD
        </Title>
      </Container>
    </Wrapper>
  );
};

export default index;
