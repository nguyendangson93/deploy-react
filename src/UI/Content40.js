import style from '../../styles/Add.module.scss';

const Content40 = (props) => {
  return (
    <div className={style.content40} style={props.style}>
        {props.children}
    </div>
  )
}

export default Content40
