import Manager from "./Manager";
import Wrapper from "../../../../UI/Wrapper/Wrapper";
import styles from "./index.module.css";
import "bootstrap/dist/css/bootstrap.min.css";
import gratitude from '../../../../../public/Logo_BNI_Gratitude.jpg'
import clarity from '../../../../../public/Logo_BNI_Clarity.jpg'
import wow from '../../../../../public/Logo_BNI_Wow.jpg'
import balance from '../../../../../public/Logo_BNI_Balance.jpg'
import ownership from '../../../../../public/Logo_BNI_Ownership.jpg'
import commitment from '../../../../../public/Logo_BNI_Commitment.jpg'
import system from '../../../../../public/Logo_BNI_System.jpg'
import confidence from '../../../../../public/Logo_BNI_Confidence.jpg'

const index = () => {
  return (
    <Wrapper>
      <div className={styles.titleContainer}>
        <h2 className={styles.title}>08 CHAPTER - VÙNG HCMC6</h2>
      </div>
      <div className={styles.managercontainer}>
        <div className="row gx-6">
          <div className="col-12 col-sm-12 col-lg-6">
            <Manager
              src={gratitude}
              chapter="BNI GRATITUDE"
              giamdoc="Nguyễn Hải Đăng"
              sdt="0907 791 164"
              link="https://hcmc6.bni.vn/hcmc-6-bni-gratitude/vi-VN/index"
            />
          </div>
          <div className="col-12 col-sm-12 col-lg-6">
            <Manager
              src={clarity}
              chapter="BNI CLARITY ONLINE"
              giamdoc="Lê Thu Huyền"
              sdt="0938 793 986"
              link="https://hcmc6.bni.vn/hcmc-6-bni-clarity-online/vi-VN/index"
            />
          </div>
        </div>

        <div className="row gx-6">
          <div className="col-12 col-sm-12 col-lg-6">
            <Manager
              src={wow}
              chapter="BNI WOW"
              giamdoc="Jack DuTien"
              sdt="0908 696 323"
              link="https://hcmc6.bni.vn/hcmc-6-bni-wow/vi-VN/index"
            />
          </div>
          <div className="col-12 col-sm-12 col-lg-6">
            <Manager
              src={balance}
              chapter="BNI BALANCE"
              giamdoc="Lê Thị Ngọc Thảo"
              sdt="0939 117 986"
              link="https://hcmc6.bni.vn/hcmc-6-bni-balance/vi-VN/index"
            />
          </div>
        </div>

        <div className="row gx-6">
          <div className="col-12 col-sm-12 col-lg-6">
            <Manager
              src={ownership}
              chapter="BNI OWNERSHIP"
              giamdoc="Tô Quý Ngọc Châu"
              sdt="0908 330 100"
              link="https://hcmc6.bni.vn/hcmc-6-bni-ownership/vi-VN/index"
            />
          </div>
          <div className="col-12 col-sm-12 col-lg-6">
            <Manager
              src={commitment}
              chapter="BNI COMMITMENT"
              giamdoc="Lê Ngọc Đăng"
              sdt="0949 823 969"
              link="https://hcmc6.bni.vn/hcmc-6-bni-commitment/vi-VN/index"
            />
          </div>
        </div>

        <div className="row gx-6">
          <div className="col-12 col-sm-12 col-lg-6">
            <Manager
              src={system}
              chapter="BNI SYSTEM"
              giamdoc="Nguyễn Thị Thanh Bình"
              sdt="0943 198 139"
              link="https://hcmc6.bni.vn/hcmc-6-bni-system/vi-VN/index"
            />
          </div>
          <div className="col-12 col-sm-12 col-lg-6">
            <Manager
              src={confidence}
              chapter="BNI CONFIDENCE"
              giamdoc="Đinh Thị Như Phượng"
              sdt="0909 926 477"
              link="https://hcmc6.bni.vn/hcmc-6-bni-confidence/vi-VN/index"
            />
          </div>
        </div>
      </div>
    </Wrapper>
  );
};

export default index;
