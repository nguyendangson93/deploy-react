
import style from '../../styles/Add.module.scss'

const Wrapper = (props) => {
  return <div style={props.wrapperStyle} className={`${style.wrapper} ${props.otherClass}`}>{props.children}</div>
}

export default Wrapper
