import { faUsers, faHandHoldingDollar, faBusinessTime } from '@fortawesome/free-solid-svg-icons'

export const statisticalList = [
        {id: 'a', counter: '184', text:'CHAPTER', icon:faUsers, nameIcon:'faUsers'},
        {id: 'b', counter: '8.332', text:'THÀNH VIÊN', icon:faUsers, nameIcon:'faUsers'},
        {id: 'c', counter: '404.395', text:'CƠ HỘI KINH DOANH', icon:faBusinessTime},
        {id: 'd', counter: '14.232 tỷ', text:'GIÁ TRỊ KINH DOANH', icon:faHandHoldingDollar},
]

export const statisticalListGlobal = [
    {id: 'e', counter: '10.700', text:'CHAPTER', icon:faUsers, nameIcon:'faUsers'},
    {id: 'f', counter: '290K', text:'THÀNH VIÊN', icon:faUsers, nameIcon:'faUsers'},
    {id: 'g', counter: '12.3M', text:'CƠ HỘI KINH DOANH', icon:faBusinessTime},
    {id: 'h', counter: '$19.2B', text:'GIÁ TRỊ KINH DOANH', icon:faHandHoldingDollar},
]

