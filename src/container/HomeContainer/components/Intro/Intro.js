import "bootstrap/dist/css/bootstrap.min.css";
import style from './Intro.module.css';
import ListStatistical from '../statistical/ListStatistical';
import { statisticalList, statisticalListGlobal } from '../statistical/statisticalList'


export default function intro_index(){
    return (
    <div className={style.wrapper} id='about'>
        <div style={{ marginBottom: '3%' }}>
            <h2 style={{ fontWeight: '700' }}>
                CHÀO MỪNG ĐẾN VỚI BNI
            </h2>
            <h2 style={{color:"#cf2030"}}>BNI - BUSINESS NETWORK INTERNATIONAL</h2>
            <p className={style.p1} >là tổ chức kết nối thương mại lớn nhất và thành công nhất trên thế giới hiện nay, được <strong>Tiến sĩ Ivan Misner</strong> sáng lập vào năm 1985.<br></br>
            Cho đến nay BNI Toàn cầu đã trao cho nhau <strong>12.2 triệu cơ hội kinh doanh</strong> (referrals) với <strong>tổng trị giá 16.9 tỉ USD</strong>.</p>
            <ListStatistical list={statisticalListGlobal} src='https://www.bni.com/' title='BNI toàn cầu' />
        </div>
        <div style={{ padding: '2% 0 2%'}}>
            <h2 className={style.head2}>BNI <font color='#cf2030'>VIỆT NAM</font></h2>
            <p>
            BNI đã phôi thai ở Việt Nam từ đầu năm 2007 nhưng do một số khó khăn do sự khác biệt về văn hóa, hội nhập
            </p>
            <p> 
            nên đến tháng 8 năm 2010 BNI mới chính thức thành lập tại Việt Nam.
            </p>
            <p> 
            Hiện tại, BNI Việt Nam đã có <strong>8,124 thành viên</strong> với <strong>185 Chapter</strong> tại các vùng, tỉnh thành lớn trên cả nước.
            </p>
            <ListStatistical list={statisticalList} src='https://bni.vn/' title='BNI Việt Nam' />
        </div>
    </div>
)}