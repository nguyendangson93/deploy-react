/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  experimental: {
    images: { allowFutureImage: true },
    urlImports: ['https://fonts.googleapis.com'],
  },
  compiler: {
    styledComponents: true,
    removeConsole: false,
  },
  images: {
    domains: ['localhost'],
  },
}

module.exports = nextConfig
