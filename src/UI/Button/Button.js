import style from '../../../styles/Add.module.scss';
import Link from 'next/link'

const Button = (props) => {
  return (
    <Link href={props.href} >
      <p style={props.style} className={style.link}>
        {props.children}
      </p>
    </Link>
  )
}

export default Button
