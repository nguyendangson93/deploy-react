import React from 'react'
import styles from './Wrapper.module.css'
const index = (props) => {
  return (
    <div className={styles.wrapper}>   
        {props.children}
    </div>
  )
}
export default index
