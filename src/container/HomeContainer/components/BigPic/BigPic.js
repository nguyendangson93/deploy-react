import Image from "next/future/image";
import style from "./BigPic.module.css";
import bigpic from "../../../../../public/big_pic_v3.jpg";
import logo from "../../../../../public/logo.png";

export default function BigPic() {
  return (
    <div style={{ textAlign: 'center',width:'100%' }}>
      <div style={{ width:'100%' }}>
        <Image
          width={1300}
          height={90}
          className={style.img}
          layout="responsive"
          src={bigpic}
          alt="logo image"
        />
      </div>
        <div className={style.line}></div>
        <div className={style["image-wrapper"]}>
          <Image
            width={300}
            height={40}
            className={style.image}
            src={logo}
          />
        </div>
      </div>
    
  );
}
