import Wrapper from "../../../../UI/Wrapper/Wrapper";
import Content50 from "../../../../UI/Content50/Content50";
import styles from "./AnnaHang.module.css";
import Image from "next/future/image";
import anna from '../../../../../public/anna.png'
1
const AnnaHang = (props) => {
  return (
    <Wrapper style={{ marginTop: "0px" }}>
      <div className={styles["header-wrapper"]}>
        <h2>
          VÙNG BNI <font color="#cf2030">HCM CENTRAL 6</font>
        </h2>
        <p className={styles.header2}>
          BNI Việt Nam có 10 vùng, riêng vùng HCMC6 nằm trong{" "}
          <font color="#cf2030">
            Top 10 vùng phát triển tốt nhất trong hệ thống BNI toàn cầu
          </font>{" "}
          do bà <strong>Anna Nguyễn Thị Bích Hằng</strong> điều hành với{" "}
          <strong>08 Chapter</strong> cùng <strong>hơn 500 thành viên</strong>{" "}
          Chủ doanh nghiệp
        </p>
      </div>

      <div className={styles["img-wrapper"]}>
        <Image
          width={605}
          height={671}
          className={styles.image}
          src={anna}
        />
      </div>

      <Content50>
        <h2>
          {props.title1} <font color="#cf2030">{props.num}</font>
        </h2>
        <p className={styles.text}>
          Bà{" "}
          <span className={styles.span}>
            <strong>ANNA</strong> Nguyễn Thị Bích Hằng
          </span>
          <br></br>
          <strong>Nguyên Tổng Giám Đốc BNI Việt Nam.</strong>
          <br></br>
          <strong>Nguyên Tổng Giám Đốc ActionCOACH Việt Nam.</strong>
          <br></br>
          <strong>Tổng Giám đốc ActionCOACH CBD Firm.</strong>
          <br></br>
          Đã điều hành và giúp ActionCOACH CBD Firm đạt giải Top 2 Thế giới (Xếp
          hạng 2/83 quốc gia).<br></br>
          Là người phụ nữ đầu tiên đưa nhà huấn luyện Việt Nam và Doanh Nghiệp
          Việt Nam được vinh doanh trên Toàn Cầu.<br></br>
          Với mục tiêu xây dựng vùng BNI HCM Central 6 trở thành vùng mang danh
          Diamond và mang lại nhiều giá trị hơn nữa dành riêng cho cộng đồng
          doanh nghiệp trong bối cảnh nền kinh tế Việt Nam đang bước vào một quỹ
          đạo phát triển mới, bà Anna Hằng Nguyễn hy vọng được đồng hành cùng
          các doanh nghiệp phát triển bền vững, góp phần vào sự phát triển chung
          & khẳng định vị thế Việt Nam trên nền kinh tế toàn cầu.
        </p>
      </Content50>
    </Wrapper>
  );
};

export default AnnaHang;
