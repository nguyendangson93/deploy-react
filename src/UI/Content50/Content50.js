import styles from './Content50.module.css';

const Content50 = (props) => {
  return (
    <div className={styles.content} >
        {props.children}
    </div>
  )
}

export default Content50
