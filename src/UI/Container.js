import style from '../../styles/Add.module.scss';

const container = (props) => {
  return (
    <div className={style.container} style={props.ortherStyle}>
        {props.children}
    </div>
  )
}

export default container
