import style from '../../../styles/Add.module.scss';
import { eventList } from './eventList';

import Image from 'next/future/image';


const ImageList = () => {
  return (
    <div className={style.slider}>
      <h2 className={style.header2}>HÌNH ẢNH SỰ KIỆN <font color='#cf2030'>ĐƯỢC TỔ CHỨC LẦN 1 - 2</font></h2>
      
      <div className={style.imageList}>
        {eventList.map((c) => (
          <div key={c.id}>
            <Image
              width={1260}
              height={513}
              layout='responsive'
              className={style.imageSlider}
              key={c.src}
              src={c.src}
            />
          </div>
        ))}
      </div>
    </div>
  );
};

export default ImageList;
