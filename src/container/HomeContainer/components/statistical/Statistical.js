import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import style from '../../../../../styles/Add.module.scss'
import { motion } from 'framer-motion'
 
const Statistical = ({item}) => {
  return (
    <>

      <div className={style.statisticalContainer}>
          <div style={{textAlign: 'center'}}>
              <motion.h2 
              initial={{ scale: 1 }}
              animate={{ scale: 1.2 }}
              transition={{ duration: 4, repeat: Infinity }}
              style={{textAlign:'center'}}
              className={style.counterService}>{item.counter}</motion.h2>
              <motion.h5
              initial={{ scale: 1.2 }}
              animate={{ scale: 0.5 }}
              transition={{ duration: 4, repeat: Infinity }}
              className={style.titleService}>{item.text}</motion.h5>
          </div>
          <div style={{textAlign: 'center', margin: 'auto', width: '100%', padding: 10}}>
              <FontAwesomeIcon style={{fontWeight: 900, color: '#B73232', width: '20%', margin: 'auto'}} icon={item.icon} />
          </div>
      </div>
    </>
  )
}

export default Statistical
