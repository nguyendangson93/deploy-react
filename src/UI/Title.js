import style from '../../styles/Add.module.scss';

const Title = (props) => {
  return (
    <h3 className={style.title} style={props.style}>
      {props.children}
    </h3>
  )
}

export default Title
