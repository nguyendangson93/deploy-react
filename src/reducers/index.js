/** @format */
import { combineReducers } from 'redux'
import { userFilterReducer } from '../container/UserContainer/reducer'

const rootReducer = combineReducers({
  user: userFilterReducer,
})

export default rootReducer
