import React from "react";
import styles from "./index.module.css";
import { NavLink } from "react-bootstrap";
import Image from "next/future/image";

const Manager = (props) => {
  return (
    <div className={styles.antCol}>
      <div className={styles.leadersCardHolder}>
        <div className={styles["img-wrapper"]}>
          <Image
            width={2339}
            height={1654}
            className={styles.profilePicture}
            src={props.src}
          />
        </div>
        <div className={styles.leaderShipCardContentHolder}>
          <h5 className={styles.chapter}>{props.chapter}</h5>
          <p className={styles.companyName}>
            <b>Giám đốc: </b>
            {props.giamdoc}
            <br></br>
            <b>Điện thoại: </b>
            {props.sdt}
            <br></br>
            <NavLink href={props.link}>
              <b>Website: </b>
              <font color="#cf2030">Click vào đây</font>
            </NavLink>
          </p>
        </div>
      </div>
    </div>
  );
};

export default Manager;
