
import AnnaHang from './components/AnnaHang/AnnaHang'
import Goal from './components/Goal/Goal'
import BigPic from './components/BigPic/BigPic'
import Intro from './components/Intro/Intro'
import dynamic from 'next/dynamic'
import Button from '../../UI/Button/Button'
import BM3 from './components/BM3/BM3'
import Clickme from './components/Hitme/Clickme'

const ImageList = dynamic(() => import('../../UI/Slider/ImageList'), {
  ssr: false,
})

const Iframe = dynamic(() => import('../../UI/Slider/Iframe'), {
  ssr: false,
})

const BNIList = dynamic(
  () => import('../HomeContainer/components/Manager/index'),
  { 
  ssr: false, 
  suspense: true,
  } 
)

const HomeContainer = () => {
  return (
      <div>
        <BigPic />        
        <Intro />
        <AnnaHang 
          title1= 'GIÁM ĐỐC VÙNG BNI HCM CENTRAL'
          num='6'
        />
        <BM3 />
        <Clickme />
        <Goal />       
        <ImageList />
        <Iframe/>
        <BNIList />
        <Button style={{position: 'fixed', bottom: 30, right: 20}} href='https://bm3.bnicbd.com/#form'></Button>
      </div>      
  )
}

export default HomeContainer
