import style from '../../../styles/Add.module.scss';
import { iframeList } from './eventList';
import Image from 'next//future/image';
import Link from 'next/link';

const Iframe = ({ src }) => {
  return (
    <div className={style.slider}>
      {
        <div className={style.iframeList}>
          {iframeList.map((item, index) => (
            <div key={item.id} className={style.iframeItem}>
              <Link href={item.href}>
                <a>
                  <Image
                    src={item.src}
                    width={700}
                    height={450}
                    layout='responsive'
                  />
                </a>
              </Link>
            </div>
          ))}
        </div>
      }
    </div>
  );
};

export default Iframe;
