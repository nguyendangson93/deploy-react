import style from "./BM3.module.css";
import styles from "../../../../../styles/Add.module.scss";
import poster from "../../../../../public/poster.jpg";
import Wrapper from "../../../../UI/Wrapper";
import Image from "next/future/image";

export default function Bm3() {
  return (
    <Wrapper>
      <div className={style.wrapper}>
        <div style={{ textAlign: "justify" }}>
          <h2 style={{ textAlign: "center", margin: "1% auto 30px" }}>
            BUSINESS MATCHING
            <strong style={{ color: "#cf2030" }}>
              {" "}
              ĐƯỢC TỔ CHỨC 06 THÁNG/LẦN
            </strong>
          </h2>
          <p>
            Business Matching là hoạt động kết nối kinh doanh thực hiện xúc tiến
            thương mại nhằm giúp các doanh nghiệp có cơ hội phát triển và tăng
            việc kết nối kinh doanh với khách hàng Sự kiện lần này của vùng{" "}
            <strong>HCM Central 6</strong> quy tụ <strong>8 Chapter</strong>,{" "}
            <strong>100 gian hàng</strong> của các thành viên sẽ đưa doanh
            nghiệp tiếp cận đến với hơn <strong>800 chủ doanh nghiệp</strong>{" "}
            tham quan trong vòng 02 ngày, với mục tiêu "Better Together - Cùng
            nhau trở nên tốt hơn" giúp các Thành viên có cơ hội tương tác và kết
            nối giao thương với nhau, nhằm tìm kiếm các đối tác kinh doanh phù
            hợp và trao đi các cơ hội kinh doanh.
          </p>
          <p>
            {" "}
            Con số dự kiến được thống kê sau Business matching mùa 1 và 2 thì
            lần kết nối sắp tới đây của mùa 3 sẽ có được{" "}
            <strong>700 Cơ hội </strong>
            được trao tại chỗ và giá trị lên đến <strong>900 Triệu</strong>.
          </p>
        </div>
        <h2 style={{ margin: "1% 0 2% 0", textAlign: "center" }}>
          Chủ đề:{" "}
          <font color="#cf2030">
            BUSINESS MATCHING - BNI BETTER TOGETHER DAYS
          </font>
        </h2>

        <div className={styles.posterContainer}>
          <Image
            width={4095}
            height={2303}
            layout="responsive"
            className={styles.poster}
            src={poster}
          />
        </div>
      </div>
    </Wrapper>
  );
}
