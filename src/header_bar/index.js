import "bootstrap/dist/css/bootstrap.min.css";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import Navbar from "react-bootstrap/Navbar";
import style from "./header_bar.module.css";
import { BiMenu } from "react-icons/bi";
import { useEffect } from "react";
import { NavLink } from "react-bootstrap";
import { useState } from "react";
import "antd/dist/antd.css";
import { Drawer } from "antd";
import { MdOutlineArrowDropDownCircle,MdManageSearch } from "react-icons/md";
import { BsFacebook } from "react-icons/bs";
import Image from 'next/future/image'
import logo from "../../public/logo.png"

function Header_bar() {
  //sticky header bar
  useEffect(() => {
    window.onscroll = function () {
      myFunction();
    };

    var headbar = document.getElementById("headbar");
    var sticky = headbar.offsetTop;

    function myFunction() {
      if (window.pageYOffset > sticky) {
        headbar.classList.add(style["sticky"]);
      } else {
        headbar.classList.remove(style["sticky"]);
      }
    }
  }, []);

  const [visible, setVisible] = useState(false); //drawer side menu variant//

  const [menu, setMenu] = useState(false);

  const [onSearch,setOnSearch] = useState(false);

  function showSearchHandle(){
    setOnSearch(!onSearch);
  }

  function Dropdown() {
    return (
      <div className={style["dropdown"]}>
        <MdOutlineArrowDropDownCircle
          className={style["cs-btn-two"]}
          onClick={() => {
            setMenu(true);
          }}
        />

        <Drawer
          title="Menu"
          placement="left"
          width="fit-content"
          closable={true}
          visible={menu}
          onClose={() => {
            setMenu(false);
          }}
        >
          <ul className={style["mobile-menu"]}>
            
          <NavLink href="/" style={{ color: "black",marginBottom:'10px' }}>
            {"Home"}
          </NavLink>
          
          <NavLink href="/#about" style={{ color: "black",marginBottom:'10px' }}>
            {"About"}
          </NavLink>
          
          

          <NavLink href="https://bm3.bnicbd.com/#form" style={{textAlign:'center' }}>
          <button className={style["btn-grad"]} style={{ width:'80px',marginLeft:'-10px' }}>
                ĐĂNG KÝ
          </button>
          </NavLink>
          
          </ul>
        </Drawer>
      </div>
    );
  }

  return (
      <Navbar id="headbar" bg="light" expand="lg" className={`col-md-12 ${style["row-h"]}`}>
        <div className={style['item-wrapper']}>
          <NavLink className={style.navlogo} href="/">
            <Image
            width={2560}
            height={1907} 
            layout="responsive"
            src={logo}
            alt="logo image"
            className={style.logo}
            />
          </NavLink>
          <Dropdown />
          <div className={style.inlinewrapper}>
            <NavLink className={style.inline} href="/">
              Home{" "}
            </NavLink>
            <NavLink className={style.inline} href="#about">
              About
            </NavLink>
            

            <NavLink href="https://bm3.bnicbd.com/#form"><button type="submit" className={style["btn-grad"]}>
                ĐĂNG KÝ SỰ KIỆN
              </button></NavLink>
              
            
            <div className={style["side-items"]}>
              <MdManageSearch  onClick={showSearchHandle} style={{width: '30px',height:'30px',margin:'10px',display:'none'}}/>
              {onSearch && (
                <Form
                  id="search"                
                  style={{ padding: "10px", display:'flex' }}
                >
                  <Form.Control
                    type="search"
                    placeholder="Search"
                    className="me-2"
                    aria-label="Search"
                  />
                  <Button variant="outline-success">Search</Button>
                </Form>
              )}
            
              <BiMenu
                className={style["cs-btn-two"]}
                style={{ marginTop: '10px' }}
                onClick={() => {
                  setVisible(true);
                }}
              />
            </div>
            <Drawer
              title="Social List"
              placement="right"
              width="fit-content"
              closable={true}
              visible={visible}
              onClose={() => {
                setVisible(false);
              }}
            >
              <NavLink href="https://www.facebook.com/BNI.HCM.Central6" style={{ color:'#42c2f5' }}><BsFacebook/></NavLink>
              
            </Drawer>
          </div>
        </div>
      </Navbar>
  );
}

export default Header_bar;
