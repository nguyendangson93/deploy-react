import Head from 'next/head'
import HomeContainer from '../src/container/HomeContainer/HomeContainer'
import Header_bar from '../src/header_bar'
import Footer from '../src/footer'


export default function Home() {

  return (
    
    <div >
    <Head>
         
          <meta httpEquiv="X-UA-Compatible" content="IE=edge"/>
          <meta name="viewport" content="width=device-width, initial-scale=1"/>
          <meta name="title" content="BNI Hồ Chí Minh CENTRAL 6"/>
          <meta name="description" content="BNI - BUSINESS NETWORK INTERNATIONAL
là tổ chức kết nối thương mại lớn nhất và thành công nhất trên thế giới hiện nay, được Tiến sĩ Ivan Misner sáng lập vào năm 1985. Cho đến nay BNI Toàn cầu đã trao cho nhau 12.2 triệu cơ hội kinh doanh (referrals) với tổng trị giá 16.9 tỉ USD"/>
          <meta name="keywords" content="BNI Hồ Chí Minh CENTRAL 6"/>
          <meta name="author" content="Viet Japan Partner"/>
          <meta property="og:title" content="BNI Hồ Chí Minh CENTRAL 6"/>
          <meta property="og:url" content=""/>
          <meta property="og:description" content="BNI - BUSINESS NETWORK INTERNATIONAL
là tổ chức kết nối thương mại lớn nhất và thành công nhất trên thế giới hiện nay, được Tiến sĩ Ivan Misner sáng lập vào năm 1985. Cho đến nay BNI Toàn cầu đã trao cho nhau 12.2 triệu cơ hội kinh doanh (referrals) với tổng trị giá 16.9 tỉ USD"/>
          <meta property="og:image" content="/thumbnail-bnic6.png"/>
          <meta itemProp="image" content="/thumbnail-bnic6.png"/>
          <meta property="og:type" content="website"></meta>
          <link href="/logo.png" rel="icon" type="image/x-icon" />
          <title>BNI Hồ Chí Minh CENTRAL 6</title>
        </Head>

        <main>
      <Header_bar />
      <HomeContainer  />
      </main>
      <Footer />
    </div>
  )
}



